       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. Benjamas.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 WEIGHT PIC 99 VALUE ZEROS .
       01 HEIGTH PIC 999 VALUE ZEROS .
       01 HEIGTH-METEER PIC 9V99 VALUE ZEROS .
       01 RESALT PIC 99V99 VALUE ZEROS .
           *>88 UNDER-WEIGHT VALUE 1 THRU 18.49 .
           *>88 NORMAL VALUE 18.50 THRU 22.99.
           *>88 CHUBBY VALUE 23 THRU 24.99.
           *>88 OBESITY VALUE 25 THRU 29.99.
           *>88 VERY-OBESE VALUE 30 THRU 99.99.
 
       01 RESALT-MESSAGE PIC X(50) VALUE SPACE .
           *>88 MS-UNDER-WEIGHT VALUE "Underweight / Skinny".
           *>88 MS-NORMAL VALUE "Normal (Healthy)".
           *>88 MS-CHUBBY VALUE "Chubby / Obesity Level 1".
           *>88 MS-OBESITY VALUE "Obesity / Obesity Level 2".
           *>88 MS-VERY-OBESE VALUE "Very obese / Grade 3 obesity".

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "Enter your weight (kg) : " WITH NO ADVANCING .
           ACCEPT WEIGHT .

           DISPLAY "Enter your height (cm) : " WITH NO ADVANCING .
           ACCEPT HEIGTH .
           
           COMPUTE HEIGTH-METEER  = HEIGTH / 100.
           COMPUTE RESALT = WEIGHT / (HEIGTH-METEER * HEIGTH-METEER  ).

           EVALUATE RESALT 
              WHEN 1 THRU 18.49 MOVE "Underweight / Skinny" TO 
              RESALT-MESSAGE   

              WHEN 18.50 THRU 22.99 MOVE "Normal (Healthy)" TO 
              RESALT-MESSAGE   

              WHEN 23 THRU 24.99 MOVE "Chubby / Obesity Level 1" TO 
              RESALT-MESSAGE   

              WHEN 25 THRU 29.99 MOVE "Obesity / Obesity Level 2" TO 
              RESALT-MESSAGE   

              WHEN 30 THRU 99.99 MOVE "Very obese / Grade 3 obesity" TO 
              RESALT-MESSAGE   
           END-EVALUATE
           
           DISPLAY "----------------------------------------------"
           DISPLAY "Your BMI is : " RESALT .
           DISPLAY "In the threshold : " RESALT-MESSAGE .

           GOBACK .

           

