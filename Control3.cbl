       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL3.
       AUTHOR. Benjamas.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 CITY-CODE PIC 9          VALUE ZERO.
           88 CITY-IS-DUBLIN       VALUE 1.
           88 CITY-IS-LINERICK     VALUE 2.
           88 CITY-IS-CORK         VALUE 3.
           88 CITY-IS-GALWAY       VALUE 4.
           88 CITY-IS-SLIGO        VALUE 5.
           88 CITY-IS-WATERFORD    VALUE 6.
           88 UNIVERSITY-CITY      VALUE 1 THRU 4.
           88 CITY-CODE-NOTVALID   VALUE 0,7,8,9.

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "ENTER A CITY CODE (1-6) - " WITH NO ADVANCING .
           ACCEPT CITY-CODE .

           IF CITY-CODE-NOTVALID 
              DISPLAY "INVALID CITY CODE ENTERED"
           ELSE 
              IF CITY-IS-LINERICK 
                 DISPLAY "HEY, WE ARE HOME."
              END-IF 

              IF CITY-IS-DUBLIN  
                 DISPLAY "HEY, WE ARE IN THE CAPITAL."
              END-IF 

              IF UNIVERSITY-CITY   
                 DISPLAY "APPLY THE RENT SURCHARGE."
              END-IF 

           END-IF .

           SET CITY-IS-DUBLIN TO TRUE 
           DISPLAY CITY-CODE 

           GOBACK .

